const express =require('express')
const bodyParser = require('body-parser');
//实例化 express
const app =express()

// 配置跨域
app.all('*', function(req, res, next) {
    // 任意
    res.header("Access-Control-Allow-Origin", "*");


    res.header("Access-Control-Allow-Headers", "X-Requested-With,Content-Type");
    // res.header("Access-Control-Allow-Headers", "*");
    // res.header("Access-Control-Allow-Headers", "X-Requested-With"); //只允许application/x-www-form-urlencoded

    res.header("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS");
    res.header("X-Powered-By", ' 3.2.1')
        //这段仅仅为了方便返回json而已
    res.header("Content-Type", "application/json;charset=utf-8");

    if(req.method == 'OPTIONS') {
        //让options请求快速返回
        res.sendStatus(200); 
    } else { 
        next(); 
    }
});





//app.use表示使用某一个中间件,插件
//下面的use只用一个的话,另外接收到的会为{}空对象;
//urlencoded 解析表单数据
app.use(bodyParser.urlencoded({extended: false}));
//解析json
app.use(bodyParser.json());


//最简单的api 接口
app.get('/user/login',(req,res)=>{
    //接收get参数 query
    console.log(req.query)
    console.log('你好')

    //处理参数
    let {user,pw} = req.query;
    if(user=='123'&&pw=='1123312'){
        res.send({err:0,msg:'登录成功'})
    }else if(user!='123'){
         res.send({err:0,msg:'账号错误'})
    }else{
        res.send({err:0,msg:'密码错误'})
    }
    
})

app.post('/user/reg',(req,res)=>{
    console.log('哈哈哈')
    //接收post 数据  消息体/请求体 req.body来接收数据
    // let {us,pw} = req.body;
    //express不能直接解析消息体,需要通过第三方的插件(body-parser)来解析
    console.log(req.body)//undefined
    console.log(req.headers['content-type'])

    res.send(JSON.stringify({err:0,msg:"注册成功",data:req.body}))
})

//监听3000端口,开启服务器
app.listen(3000,()=>{
    console.log('Server start')
})
