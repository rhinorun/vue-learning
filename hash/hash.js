// 哈希算法,生成唯一字符串
function hashStringToInt(key,tableSize){
  let hash = 17;
  // charCodeAt() 方法可返回指定位置的字符的 Unicode 编码(ascii码)。这个返回值是 0 - 65535 之间的整数。
  // 取余是让hash这个值始终小于哈希表的长度
  for (let i = 0; i < key.length; i++) {
    hash = (13 * hash * key.charCodeAt(i)) % tableSize
    console.log(hash)
  }
  return hash
}
console.log('abcd'.charCodeAt(0)) // a的ascii码为97
console.log('abcd'.charAt(0)) // 输出a
// 哈希表
class HashTable {
  table = new Array(33);

  getItem = key =>{
    const idx = hashStringToInt(key, this.table.length);
    if(!this.table[idx]){
      return null
    }
    return this.table[idx]
  }

  setItem = (key, val) =>{
    const idx = hashStringToInt(key, this.table.length);
    console.log(idx)
    this.table[idx] =val
  }
}
const myTable = new HashTable();
myTable.setItem('firstName','rhino')
// myTable.setItem('lastName','lin')
console.log(myTable.getItem('firstName'))