import { createApp } from 'vue'
import App from './App.vue'
// import App from './base/1_setup.vue'
// import App from './base/2_todo.vue'
// import App from './base/3_todo_ref.vue'
// import App from './base/4_ref.vue'
// import App from './base/5_toRefs.vue'
// import App from './base/6_父传子.vue'
// import App from './base/7_子传父.vue'
// import App from './base/8_生命周期.vue'
// import App from './base/9_computed.vue'
// import App from './base/10_watch.vue'

import router from './router'
import store from './store'

createApp(App).use(store).use(router).mount('#app')
