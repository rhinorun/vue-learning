// import { createRouter, createWebHashHistory,createWebHistory } from 'vue-router'
import { createRouter, createWebHashHistory } from 'vue-router'
import Film from '../base/view/Film.vue'
import Cinema from '../base/view/Cinema.vue'
import Detail from '../base/view/Detail.vue'

const routes = [
  {
    path: '/film',
    name: 'Film',
    component: Film
  },
  {
    path: '/detail',
    name: 'Detail',
    component: Detail
  },
  {
    path: '/cinema',
    name: 'Cinema',
    component: Cinema
  },
  {
    path: '/',
    redirect: '/film'
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

// 路由拦截
router.beforeEach((to, from, next) => {
  if (to.fullPath === '/film') {
    console.log('拦截成功')
  }
  next()
})
export default router
