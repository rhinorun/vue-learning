import { ref } from 'vue'
import axios from 'axios'

function useURLAixos (url: string) {
  const result = ref(null)
  const loading = ref(true)
  const loaded = ref(false)
  const error = ref(null)

  axios.get(url).then(res => {
    console.log(res.data)
    loading.value = false
    loaded.value = true
    result.value = res.data
  }).catch(err => {
    loading.value = false
    loaded.value = true
    error.value = err
  })

  return { result, loading, loaded, error }
}

export default useURLAixos
