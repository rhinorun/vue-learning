import { ref } from 'vue'

const nowTime = ref<string>('00:00:00')

const addZero = (num: number | string): number | string => {
  return num < 10 ? `0${num}` : num
}

const getNowTime = () => {
  const now = new Date()
  nowTime.value = `${addZero(now.getHours())}:${addZero(now.getMinutes())}:${addZero(now.getSeconds())}`
  setTimeout(getNowTime, 1000)
}

export default { nowTime, getNowTime }
