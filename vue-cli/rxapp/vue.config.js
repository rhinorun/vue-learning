// 关闭eslint, 反向代理(跨域).....
module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? '/testvue/'
    : '/',
  outputDir: 'dist',
  assetsDir: 'static',
  indexPath: 'index.html',
  lintOnSave: true,
  devServer: {
    proxy: {
      '/ajax': {
        target: 'https://m.maoyan.com',
        changeOrigin: true
      }
    }
  }
}
