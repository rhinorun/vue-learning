import Vue from 'vue'
import VueRouter from 'vue-router'
import Film from '../views/Film.vue'
import Center from '../views/Center.vue'
import NowPlaying from '../views/Film/NowPlaying.vue'
import ComingSoon from '../views/Film/ComingSoon.vue'
import Detail from '../views/Film/Detail.vue'
import CinemaSearch from '../views/CinemaSearch.vue'

Vue.use(VueRouter) // 注册路由模块 创建了全局组件router-view

const routes = [
  {
    path: '/film',
    name: 'Film',
    component: Film,
    children: [
      {
        path: '/film/nowplaying',
        name: 'Nowplaying',
        component: NowPlaying
      },
      {
        path: '/film/comingsoon',
        name: 'ComingSoon',
        component: ComingSoon
      },
      {
        path: '',
        redirect: '/film/nowplaying'
      }
    ]
  },
  {
    path: '/detail/:myId', // 动态路由
    component: Detail
  },
  {
    path: '/detail2', // 动态路由
    name: 'rxDetail',
    component: Detail
  },
  {
    path: '/cinema',
    name: 'Cinema',
    component: () => import('../views/Cinema.vue')
  },
  {
    path: '/cinema/search',
    name: 'Search',
    component: CinemaSearch
  },
  {
    path: '/city',
    name: 'City',
    component: () => import('../views/City.vue')
  },
  {
    path: '/center',
    name: 'Center',
    component: Center
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/Login.vue')
  },
  {
    path: '*',
    redirect: '/film'
  }
]

const router = new VueRouter({
  mode: 'hash',
  // base: process.env.BASE_URL,
  routes
})
router.onError((error) => {
  const pattern = /Loading chunk (\d)+ failed/g
  const isChunkLoadFailed = error.message.match(pattern)
  const targetPath = router.history.pending.fullPath
  if (isChunkLoadFailed) {
    router.replace(targetPath)
  }
})
// 全局路由守卫
// router.beforeEach((to, from, next) => {
//   // 路径集合
//   const auth = ['/center', '/order', '/money', '/card']

//   if (auth.includes(to.fullPath) && !localStorage.getItem('token')) {
//     next({ name: 'Login' })
//   } else {
//     next()
//   }
// })

export default router
