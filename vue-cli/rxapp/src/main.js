import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axiosConfig from '@/util/http'
import VueTouch from 'vue-touch'
Vue.use(VueTouch)
Vue.use(axiosConfig)

// 配置全局axios

// 生产环境打不打log
Vue.config.productionTip = false
// 导入App这个模块,并渲染这个模块到对应挂载的节点
// new Vue({el:'#app'}) 先创建vue实例再挂载到app节点上

new Vue({
  router, // this.$router
  store, // this.$store
  render: h => h(App)
}).$mount('#app')
