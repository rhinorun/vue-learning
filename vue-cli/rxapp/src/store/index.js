import Vue from 'vue'
import Vuex from 'vuex'
import CinemaModule from './module/CinemaModule'
import CityModule from './module/CityModule'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

// Store全局对象
export default new Vuex.Store({
  plugins: [createPersistedState()],
  state: {
  },
  // 集中式的修改状态
  mutations: {
  },
  // 异步
  actions: {
  },
  // 各个模块
  modules: {
    CityModule,
    CinemaModule
  }
})
