const module = {
  namespaced: true, // 开启命名空间
  state: {
    cityId: '310100',
    cityName: '上海'
  },
  // 集中式的修改状态
  mutations: {
    changeCity (state, data) {
      state.cityId = data.id
      state.cityName = data.name
    }
  },
  actions: {

  }
}
export default module
