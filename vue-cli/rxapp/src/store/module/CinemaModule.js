import Vue from 'vue'
const module = {
  namespaced: true,
  state: {
    cinemaList: []
  },
  // 集中式的修改状态
  mutations: {
    clearCinemaList (state) {
      state.cinemaList = []
    },
    setCinemaList (state, value) {
      state.cinemaList = value
    }
  },
  // 异步
  actions: {
    getCinemaList (store, cityId) {
      return Vue.axios({
        url: `/gateway?cityId=${cityId}&ticketFlag=1&k=2035581`,
        headers: {
          'X-Host': 'mall.film-ticket.cinema.list'
        }
      }).then((res) => {
        console.log(res.data)
        store.commit('setCinemaList', res.data.data.cinemas)
      })
    }
  }
}
export default module
