import { createApp } from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router/index";
import store from "./store/index";
import installElementPlus from "./plugins/element";

// createApp(App)
//   .use(store)
//   .use(router)
//   .mount("#app");
const app = createApp(App);
installElementPlus(app);
app.use(router);
app.use(store);

app.mount("#app");
