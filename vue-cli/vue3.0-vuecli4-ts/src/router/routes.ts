// 路由配置
const Routes = [{
    path: '/',
    name: 'Home',
    component: () =>
        import ('../views/pages/Home.vue'),
},{
    path: '/about',
    name: 'About',
    component: () =>
        import ('../views/pages/About.vue'),
},{
    path: '/test',
    name: 'Test',
    component: () =>
        import ('../views/pages/Test.vue'),
}];
export default Routes;