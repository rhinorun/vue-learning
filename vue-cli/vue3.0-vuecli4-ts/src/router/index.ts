import { createRouter, createWebHashHistory, RouteRecordRaw } from "vue-router";
import Router from './routes';

const routes: Array<RouteRecordRaw> =Router;

const router = createRouter({
  history: createWebHashHistory(),
  routes
});

export default router;
