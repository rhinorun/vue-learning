import TestStateTypes from "@/store/modules/test/interface";
import RootStateTypes from "@/store/interface";
import { Module } from "vuex";

const testModule: Module<TestStateTypes, RootStateTypes> = {
  namespaced: process.env.NODE_ENV !== "production",
  state: {
    test: "something"
  },
  mutations: {
    updateTestData(state: any, newData: string) {
      state.test = newData;
    }
  },
  actions: {}
};
export default testModule;
