// 子模块 sate类型引入
import testStateTypes from "@/store/modules/test/interface";

// root级 sate 类型定义
export default interface RootStateTypes {
  testData: string;
}

// vuex 所有sate类型定义集成
export interface AllSateTypes extends RootStateTypes {
  testModule: testStateTypes;
}
