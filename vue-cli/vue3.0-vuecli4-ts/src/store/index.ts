import { InjectionKey } from "vue";
import {
  createStore,
  createLogger,
  Store,
  useStore as baseUseStore
} from "vuex";
import RootStateTypes from "@/store/interface";
import RootStates from "@/store/state";

// 导入模块
import testModule from "@/store/modules/test/test";

const debug = process.env.NODE_ENV !== "production";
export default createStore<RootStateTypes>({
  state: RootStates,
  getters: {},
  mutations: {},
  actions: {},
  modules: {
    testModule
  },
  strict: debug,
  plugins: debug ? [createLogger()] : []
});

export const key: InjectionKey<Store<RootStateTypes>> = Symbol("vue-store");
export function useStore() {
  return baseUseStore(key);
}
