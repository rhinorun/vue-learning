# vue3.0-vuecli4-ts 快速尝鲜

vuecli4、vue3.0、typeScript、vuex、vuex-Modlue、vuex-class、vue-class-component、vue-property-decorator、vue-router、element-plus

### 项目安装 Project setup

```
npm install
```

### 项目启动 Compiles and hot-reloads for development

```
npm run serve  // 初始默认
```

```
npm run dev    // 开发启动
```

### 生产构建 Compiles and minifies for production

```
npm run build
```

### 语法检测 Lints and fixes files

```
npm run lint
```

### 自定义配置 Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

### 目录结构

```
vue3.0-vuecli4-ts                                      //
├─ babel.config.js                                     //
├─ package-lock.json                                   //
├─ package.json                                        //
├─ public                                              //
│  ├─ config                                           //
│  ├─ favicon.ico                                      //
│  ├─ img                                              //
│  │  ├─ icons                                         //
│  │  └─ images                                        //
│  ├─ index.html                                       //
│  └─ robots.txt                                       //
├─ README.md                                           //
├─ src                                                 //
│  ├─ api                                              //
│  ├─ App.vue                                          //
│  ├─ assets                                           //
│  │  ├─ logo.png                                      //
│  │  ├─ script                                        //
│  │  └─ styles                                        //
│  ├─ components                                       //
│  │  ├─ layout                                        //
│  │  ├─ script                                        //
│  │  └─ template                                      //
│  ├─ main.ts                                          //
│  ├─ plugins                                          //
│  │  └─ element.ts                                    //
│  ├─ registerServiceWorker.ts                         //
│  ├─ router                                           //
│  │  ├─ index.ts                                      //
│  │  └─ routes.ts                                     //
│  ├─ shims-vue.d.ts                                   //
│  ├─ store                                            //
│  │  ├─ index.ts                                      //
│  │  ├─ interface.ts                                  //
│  │  ├─ modules                                       //
│  │  │  ├─ login                                      //
│  │  │  └─ test                                       //
│  │  │     ├─ interface.ts                            //
│  │  │     └─ test.ts                                 //
│  │  └─ state.ts                                      //
│  └─ views                                            //
│     ├─ pages                                         //
│     │  ├─ About.vue                                  //
│     │  ├─ Home.vue                                   //
│     │  └─ Test.vue                                   //
│     ├─ script                                        //
│     └─ styles                                        //
└─ tsconfig.json                                       //

```
