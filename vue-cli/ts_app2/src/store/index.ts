import Vue from 'vue'
import Vuex from 'vuex'
import MyStoreState from './module/MyStore'
Vue.use(Vuex)

interface MyRootState{
  myStore: MyStoreState;
}
export default new Vuex.Store<MyRootState>({})
