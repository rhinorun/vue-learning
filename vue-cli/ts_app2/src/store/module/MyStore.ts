/*
  {
    store:{
      count = 1
      list = [1,2,3,4]
    }
  }
*/
import { Module, VuexModule, Mutation, Action, getModule } from 'vuex-module-decorators'
import store from '../index'

const getList = () => {
  return new Promise<number[]>((resolve) => {
    setTimeout(() => {
      resolve([1, 2, 3, 4])
    }, 3000)
  })
}

export interface MyStoreState {
  count: number;
  list: number[];
}
@Module({
  name: 'myStore',
  dynamic: true, // 动态模块
  store
})
export default class MyStore extends VuexModule implements MyStoreState {
  count = 1

  list: number[] = []

  // getter
  get filterList () {
    return this.list.filter(item => item > 2)
  }

  // Mutation
  @Mutation
  addCount (num: number) {
    this.count += num
  }

  @Mutation
  updateList (list: number[]) {
    this.list = list
  }

  @Action
  async getList () {
    const arr: number[] = await getList()
    this.updateList(arr)
  }
}
export const MyStoreOut = getModule(MyStore)
