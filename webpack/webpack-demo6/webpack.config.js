const path = require('path') //引入node模块
// 安装依赖
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports={
  // 入口文件
  entry: "./src/index.js",
  // 输出文件
  output: {
    // 输出文件名称
    filename: "bundle.js",
    path: path.resolve(__dirname, "dist"),
    publicPath: "./"
  },
  // 设置开发模式 production(生产模式)
  mode: "development",
  // loader的配置
  module: {
    // 对某种格式的文件进行转换处理
    rules: [
      {
        // 正则匹配文件类型
        test: /\.css$/,
        // 指定loader,从下往上执行
        use: [
          "style-loader", // 将转换为js的css插入到html的style里
          "css-loader"  // 将css文件转换为js
        ]
      },
      {
        // 配置图片的解析
        // 匹配图片文件
        test: /\.(jpg|png|gif|webp)$/,
        loader: 'url-loader',
        // 图片小于8kb. base64处理, 减少请求数量,会使体积更大
        options: {
          limit: 8*1024,
          // 关闭url-loader es6模块化解析,因为回合html-loader冲突
          esModule: false,
          //[hash:10]取前10位hash,[ext]文件拓展名
          name: '[hash:10].[ext]'
        }
      },
      {
        // 解析html
        test: /\.html$/,
        loader: 'html-loader'
      }
    ]
  },
  // plugins插件配置
  plugins: [
    // 模板位置
    new HtmlWebpackPlugin({
      template: './src/index.html'
    })
  ],
  // 
  devServer: {
    // 项目构建的路径
    contentBase: path.resolve(__dirname, "dist"),
    // 是否启动gzip进行编码压缩,使浏览器开启更快
    compress: true,
    // 端口号
    port: 3000,
    // 自动打开浏览器
    open: true
  }
}