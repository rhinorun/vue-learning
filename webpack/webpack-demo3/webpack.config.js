const path = require('path') //引入node模块
console.log(path.resolve(__dirname, 'dist'))

module.exports={
  // 入口文件
  entry: "./src/index.js",
  // 输出文件
  output: {
    // 输出文件名称
    filename: "bundle.js",
    /* 
      输出路径 (用node path.resolve获取当前文件夹的绝对路径,__dirname变量都表示当前运行的js文件所在的目录，它是一个绝对路径,所以是将两个当前文件的文件夹地址拼接上文件夹名,
      即当前目录下的dist)
    */
    path: path.resolve(__dirname, "dist"),
  },
  // 设置开发模式 production(生产模式)
  mode: "development",
  // loader的配置
  module: {
    // 对某种格式的文件进行转换处理
    rules: [
      {
        // 正则匹配文件类型
        test: /\.css$/,
        // 指定loader,从下往上执行
        use: [
          "style-loader", // 将转换为js的css插入到html的style里
          "css-loader"  // 将css文件转换为js
        ]
      }
    ]
  }
}