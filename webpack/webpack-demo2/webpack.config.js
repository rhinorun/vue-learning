const path = require('path') //引入node模块
console.log(path.resolve(__dirname, 'dist'))

module.exports={
  // 入口文件
  entry: "./src/index.js",
  // 输出文件
  output: {
    // 输出文件名称
    filename: "bundle.js",
    /* 
      输出路径 (用node path.resolve获取当前文件夹的绝对路径,__dirname变量都表示当前运行的js文件所在的目录，它是一个绝对路径,所以是将两个当前文件的文件夹地址拼接上文件夹名,
      即当前目录下的dist)
    */
    path: path.resolve(__dirname, "dist"),
  },
  // 设置开发模式 production(生产模式)
  mode: "development"
}